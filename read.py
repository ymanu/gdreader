# author: Manuel Mayssen
# date: 17.05.2021
# email: manuel.mayssen@gmail.com

# IMPORTS
import os
import datetime
import PyPDF2 as pypdf
import xlwt
from colorama import Fore, Back, Style

# GLOBALS
# Version
versionNumber = "0.0.5"
year = "2021"
# Excel file
excelFile = xlwt.Workbook()
worksheet = 0
worksheets = []
date = datetime.datetime.now().strftime("%d_%m_%Y")
outputSuffix = ".xls"
filenameSeparator = "_"
outputName = "Stufendaten" + filenameSeparator + date + outputSuffix
# Leiter & Kids Gesundheitsblätter
kidsFieldsCount = 47
leadersFieldsCount = 31
# Separate Stufen
separateStufenFlag = False
separateCounters = [0, 0, 0, 0, 0, 0]
separateIndices = {
    "WiWö" : 0,
    "GuSp" : 1,
    "CaEx" : 2,
    "RaRo" : 3,
    "Missing" : 4,
    "Leiter" : 5
}
allIndices = {
    "Alle" : 0,
    "Leiter" : 1
}

# Map
fieldToExcelMapKids = {
    "Stufe" : 0,
    "Name Kind" : 1,
    "Vorname Kind" : 2,
    "GebTag" : 3,
    "GebMonat" : 4,
    "GebJahr" : 5,
    "Straße" : 6,
    "PLZ" : 7,
    "Ort" : 8,
    "Geschlecht" : 9,
    "Email" : 10,
    "Tel1" : 11,
    "Diät" : 12,
    "Top-Jugend" : 13,
    "Klasse" : 14,
    "allein nachhause" : 15,
    "abgeholt von Anderen" : 16,
    "Unterstützung" : 17
}

fieldToExcelMapLeaders = {
    "Geschlecht" : 0,
    "Vorname Kind" : 1,
    "Name Kind" : 2,
    "GebTag" : 3,
    "GebMonat" : 4,
    "GebJahr" : 5,
    "Straße" : 6,
    "Ort" : 7,
    "PLZ" : 8,
    "Email" : 9,
    "Leiter-Tel" : 10,
    "Notfallkontakt-Nr" : 11,
    "Opt Out" : 12,
    "chronische Krankheiten" : 13,
    "regelmäßige Medikamente" : 14,
    "Umstände" : 15,
    "Diät" : 16,
    "Versicherung" : 17,
    "Tetanus" : 18,
    "Tetanus Jahr" : 19,
    "Nachimpfen ?" : 20,
    "Zecken" : 21,
    "Zecken Jahr" : 22,
    "Fotos" : 23,
    "Öffi-Ermäßigungen" : 24
}

# FUNCTIONS
# Helper functions
def getNumOfTotalPDFs():
    count = 0
    for file in os.listdir(os.getcwd()):
        if file.endswith(".pdf"):
            count = count + 1

    return count


def isLeaderGesundheitsdatenblatt(fields):
    global leadersFieldsCount
    return len(fields) == leadersFieldsCount


def validatePDF(fields):
    if fields != None:
        if len(fields) == kidsFieldsCount or len(fields) == leadersFieldsCount:
            return True
        
    return False


def getConcatenatedFields(fieldA, fieldB, fields):
    temp = ""
    if '/V' in fields[fieldA]:
        temp = temp + str(fields[fieldA]['/V']) + "; "
    if '/V' in fields[fieldB]:
        temp = temp + str(fields[fieldB]['/V']) + "; "
    return temp


# Program functions
def writeBeautifiedData(fields, ws, rowIndex):
    global fieldToExcelMapKids
    global fieldToExcelMapLeaders
    excelField = fieldToExcelMapLeaders if isLeaderGesundheitsdatenblatt(fields) else fieldToExcelMapKids

    for f in fields:
        if '/V' in fields[f] and len(fields[f]['/V']) > 0:
            # gender is separated in PDF file and value will be written here
            if f == "M" or f == "W":
                ws.write(rowIndex, excelField["Geschlecht"], f)
            # beautify "Stufen" value
            elif f == "Stufen":
                stufe = ""
                if 'w' in fields[f]['/V'].lower():
                    stufe = "WiWö"
                if 'g' in fields[f]['/V'].lower():
                    stufe = "GuSp"
                if 'c' in fields[f]['/V'].lower():
                    stufe = "CaEx"
                if 'r' in fields[f]['/V'].lower():
                    stufe = "RaRo"
                ws.write(rowIndex, excelField["Stufe"], stufe)            
            # Concatenated fields
            elif f == "Tel1" or f == "Tel2":
                ws.write(rowIndex, excelField["Tel1"], getConcatenatedFields("Tel1", "Tel2", fields))
            elif f == "Diät" or f == "Allergien":
                ws.write(rowIndex, excelField["Diät"], getConcatenatedFields("Diät", "Allergien", fields))
            elif f == "abgeholt von Anderen" or f == "Anmerkungen Abholung":
                ws.write(rowIndex, excelField["abgeholt von Anderen"], getConcatenatedFields("abgeholt von Anderen", "Anmerkungen Abholung", fields))
            elif f == "Unterstützung" or f == "Unterstützung bei":
                ws.write(rowIndex, excelField["Unterstützung"], getConcatenatedFields("Unterstützung", "Unterstützung bei", fields))
            elif (f == "Versicherungsnr Kind" or f == "Versicherung") and isLeaderGesundheitsdatenblatt(fields):
                ws.write(rowIndex, excelField["Versicherung"], getConcatenatedFields("Versicherungsnr Kind", "Versicherung", fields))
            elif f == "Notfallkontakt-Nr" or f == "Notfall-Wer":
                ws.write(rowIndex, excelField["Notfallkontakt-Nr"], getConcatenatedFields("Notfallkontakt-Nr", "Notfall-Wer", fields))
            else:
                # standard behaviour - write value to cell
                if f in excelField:
                    if fields[f]['/V'][0] == "/":
                        ws.write(rowIndex, excelField[f],  str(fields[f]['/V'])[1:])
                    else:
                        ws.write(rowIndex, excelField[f], str(fields[f]['/V']))
        else:
            # Fill cells which are obligatory with red
            if f in excelField and excelField[f] >= 1 and excelField[f] <= 11 and not isLeaderGesundheitsdatenblatt(fields):
                style=xlwt.easyxf('pattern: pattern solid, fore_colour red;')
                ws.write(rowIndex, excelField[f], "FEHLT", style=style)


def writeAllStufen(fields, rowIndex):
    global worksheets
    currentIndex = allIndices["Alle"]
    ws = worksheets[currentIndex]
    if isLeaderGesundheitsdatenblatt(fields):
        currentIndex = allIndices["Leiter"]
    
    ws = worksheets[currentIndex]
    separateCounters[currentIndex] = separateCounters[currentIndex] + 1
    rowIndex = separateCounters[currentIndex]

    writeBeautifiedData(fields, ws, rowIndex)


def writeSeparateStufen(fields, rowIndex):
    global worksheets
    global separateCounters

    currentIndex = separateIndices["WiWö"]
    ws = worksheets[currentIndex]
    # Pick correct Stufe and set index

    if isLeaderGesundheitsdatenblatt(fields):
        currentIndex = separateIndices["Leiter"]
    else:
        if '/V' in fields["Stufen"]:
            if "w" in fields["Stufen"]['/V'].lower():
                currentIndex = separateIndices["WiWö"]
            if "g" in fields["Stufen"]['/V'].lower():
                currentIndex = separateIndices["GuSp"]
            if "c" in fields["Stufen"]['/V'].lower():
                currentIndex = separateIndices["CaEx"]
            if "r" in fields["Stufen"]['/V'].lower():
                currentIndex = separateIndices["RaRo"]
        else:
            currentIndex = separateIndices["Missing"]

    ws = worksheets[currentIndex]
    separateCounters[currentIndex] = separateCounters[currentIndex] + 1
    rowIndex = separateCounters[currentIndex]

    writeBeautifiedData(fields, ws, rowIndex)


def writeToExcel(fields, rowIndex):
    global separateStufenFlag
    if separateStufenFlag:
        writeSeparateStufen(fields, rowIndex)
    else:
        writeAllStufen(fields, rowIndex)


def readFromPDFAndWriteToExcel(path, rowIndex):
    pdfobject=open(path,'rb')
    pdf=pypdf.PdfFileReader(pdfobject)
    fields=pdf.getFields()
    if validatePDF(fields):
        writeToExcel(fields, rowIndex)
        return True
    else:
        print(Fore.RED + "Error! ", path, " is not a valid Gesundheitsdatenblatt or is incompatible with the scripts version..." + Style.RESET_ALL)
        return False


def prepareExcelFile():
    global excelFile
    global worksheet
    global worksheets
    global separateStufenFlag
    global separateIndices
    global allIndices

    styleString = "font: bold on;"
    style = xlwt.easyxf(styleString)

    # choose indices based on user choice
    indices = allIndices
    if separateStufenFlag:
        indices = separateIndices

    # create sheets based on indices
    for key in indices:
        worksheets.append(excelFile.add_sheet(key, cell_overwrite_ok=True))
        if key == "Leiter":
            for element in fieldToExcelMapLeaders:
                if element == "Diät":
                    worksheets[indices["Leiter"]].write(0, fieldToExcelMapLeaders[element], "Diät, Allergien", style=style)
                else:
                    worksheets[indices["Leiter"]].write(0, fieldToExcelMapLeaders[element], element, style=style)
        else:
            for element in fieldToExcelMapKids:
                if element == "Diät":
                    worksheets[indices[key]].write(0, fieldToExcelMapKids[element], "Diät, Allergien", style=style)
                elif element == "abgeholt von Anderen":
                    worksheets[indices[key]].write(0, fieldToExcelMapKids[element],  "abgeholt von Anderen, Anmerkung Abholung", style=style)
                elif element == "Unterstützung":
                    worksheets[indices[key]].write(0, fieldToExcelMapKids[element],  "Unterstützung, Unterstützung bei", style=style)
                else:
                    worksheets[indices[key]].write(0, fieldToExcelMapKids[element], element, style=style)


def closeExcelFile():
    global excelFile
    global outputName
    excelFile.save(outputName)
    print("Writing to xls done!")
    print("Saved file under: " + outputName)


def startDataTransfer():
    rowIndex = 1
    totalPDFs = getNumOfTotalPDFs()
    for file in os.listdir(os.getcwd()):
        if file.endswith(".pdf"):
            print("File " + str(rowIndex) + "/" + str(totalPDFs) + ":", os.path.join(os.getcwd(), file))
            if readFromPDFAndWriteToExcel(os.path.join(os.getcwd(), file), rowIndex):
                rowIndex = rowIndex + 1


def askSeparateStufen():
    global separateStufenFlag
    answer = input("Should the \"Stufen\" get separated into different sheets? (y/N)")
    if answer.lower() == "y" or answer.lower() == "yes":
        separateStufenFlag = True


def printIntro():
    print(Fore.BLACK + Back.WHITE + "            _                    _           ")
    print("           | |                  | |          ")
    print("   __ _  __| |_ __ ___  __ _  __| | ___ _ __ ")
    print("  / _` |/ _` | '__/ _ \/ _` |/ _` |/ _ \ '__|")
    print(" | (_| | (_| | | |  __/ (_| | (_| |  __/ |   ")
    print("  \__, |\__,_|_|  \___|\__,_|\__,_|\___|_|   ")
    print("   __/ |                                     ")
    print("  |___/                  ver. " + versionNumber + " " + year + "     " + Style.RESET_ALL)
    print()
    print("Welcome to the absolutely CCC Gesundheitsdatenblatt Reader!")
    print("made for Pfadfindergruppe Hinterbrühl")
    print("by Manuel Mayssen")
    print()


def main():
    printIntro()
    askSeparateStufen()

    prepareExcelFile()
    startDataTransfer()
    closeExcelFile()
    input("Press enter to exit...")
# ---- MAIN ----
main()