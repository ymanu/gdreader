# GesundheitsdatenReader

A simple data reader script to automatically create an .xls (Excel Document) file from all PDFs in the same directory as the script is.

**Usage**
- Download and move the reader file (.exe or .py) to a directory on your PC
- Put the Gesundheitsdaten PDFs in the same folder
- Run the script/exe file 

    `python read.py`
- Any PDF or other file that isn't compatible with the script will be skipped

**Requirements**

Python 3.7.x-3.9.x
Install python modules `PyPDF2`, `colorama` and `xlwt`

`pip install PyPDF2, xlwt, colorama`


_Usage restricted to members of the Pfadfindergruppe Hinterbrühl - Johannes v. Matha_
